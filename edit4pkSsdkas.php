<?php 
    require_once('connect.php');

   

    if (isset($_REQUEST['update_id'])) {
        try {
            $id = $_REQUEST['update_id'];
            $select_stmt = $db->prepare("SELECT * FROM tb_goods WHERE id = :id");
            $select_stmt->bindParam(':id', $id);
            $select_stmt->execute();
            $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);
        } catch(PDOException $e) {
            $e->getMessage();
        }
    }

    if (isset($_REQUEST['btn_update'])) {
        $tracks = $_REQUEST['tracks'];

        if (empty($tracks)) {
            $errorMsg = "Please enter tracks";
       
        } else {
            try {
                
                if (!isset($errorMsg)) {
                    $update_stmt = $db->prepare("UPDATE tb_goods SET tracks = :track_up WHERE id = :id");
                    $update_stmt->bindParam(':track_up', $tracks);
                    $update_stmt->bindParam(':id', $id);

                    if ($update_stmt->execute()) {
                        $updateMsg = "update successfully...รอ3วิ";
                        ?>
                        <meta http-equiv="refresh" content="3;url=./adminshoweditpk.php">
                            <?php
                    }
                }
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<script data-ad-client="ca-pub-4542219075817151" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script src="assets/js/jquery.min.js?_=5f06309b462b2"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<script type="text/javascript" src="ajax/porserver.js"></script>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
    <!-- <link rel="stylesheet" type="text/css" href="css/default.css"> -->
    <link type="text/css" rel="stylesheet" href="css/layout.css?v=1007" media="screen,projection" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit:300&display=swap">
    <link rel="stylesheet" href="dist/sweetalert.css">
    <script src="dist/sweetalert.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <!--script src="https://member.ufagold.bet/js/common.js"></script-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="shortcut icon" href="images/ppp.ico">

    <style>
        body {
            font-family: 'Kanit', serif;
            font-size: 17px;
            padding: 8px;
        }

        * {
            box-sizing: border-box;
        }

        .row {
            display: -ms-flexbox;
            /* IE10 */
            display: flex;
            -ms-flex-wrap: wrap;
            /* IE10 */
            flex-wrap: wrap;
            margin: 0 -16px;
        }

        .col-25 {
            -ms-flex: 25%;
            /* IE10 */
            flex: 25%;
            padding: 1px;
        }

        .col-50 {
            -ms-flex: 50%;
            /* IE10 */
            flex: 50%;
            padding: 1px;
        }

        .col-75 {
            -ms-flex: 75%;
            /* IE10 */
            flex: 75%;
            padding: 1px;
        }

        .col-25,
        .col-50,
        .col-75 {
            padding: 1px;
        }

        .container1 {
            background-color: #4c1212;
            padding: 5px 10px 15px 15px;
            /* border: 1px solid lightgrey; */
            border-radius: 3px;
        }

        input[type=text] {
            width: 100%;
            margin-bottom: 20px;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        label {
            margin-bottom: 10px;
            display: block;
        }

        .icon-container {
            margin-bottom: 20px;
            padding: 7px 0;
            font-size: 24px;
        }

        .btn1 {
            background-color: #d60000;
            color: white;
            padding: 12px;
            margin: 10px 0;
            border: none;
            width: 100%;
            border-radius: 3px;
            cursor: pointer;
            font-size: 17px;
        }

        .btn:hover {
            background-color: #45a049;
        }

        a {
            color: #2196F3;
        }

        hr {
            border: 1px solid lightgrey;
        }

        span.price {
            float: right;
            color: grey;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
        @media (max-width: 800px) {
            .row {
                flex-direction: column-reverse;
            }

            .col-25 {
                margin-bottom: 20px;
            }
        }
    </style>



</head>


<body>

    <p align="center"><img src="images/logo.png" width="40%"></p>
    <div class="bg-1">
        
<div class="topnav" id="myTopnav">
  <div class="col-md-12">
  <a></a>
  <a href="index.php">หน้าแรก</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div></div>

        <div class="container">
            <div class="col-md-12  bg-3">
                <div class="col-md-12  bg-2">
                    <!-- <div class="col-md-12 bg-1l"> -->
                    <!-- <p align="center"><img src="img/UfaUCL06.png" class="img img-responsive mt-3" width="300" alt="Card image cap"><br></p> -->
                    
<link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Kanit:300&display=swap">
    <style>
      body {
        font-family: 'thaisansneue', serif;
      }
    </style><meta charset='utf-8'><div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>

    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/HULK_WEBSITE_1.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_2.jpg" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="images/HULK_WEBSITE_3.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_4.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_5.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_6.jpg"  style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
                    <!-- <div class="col-md-4">
                    <br>
                    <img src="images/Marvel_0012.jpg" width="100%"><br>
                    <img src="images/Marvel_0004.jpg" width="100%"><br>
                    <img src="images/Marvel_0013.jpg" width="100%">
                    <br><br>
                </div> -->
                    <div class="col-md-2 pc">
                        <!-- <div class="col-md-4 "> -->
                        <br>
                        <img src="images/0019.jpg" width="100%"><br>
                        <img src="images/Banner07.5.jpg" width="100%"><br>
                        <img src="images/Banner06.jpg" width="100%"><br>
                        <img src="images/20200001.jpg" width="100%"><br>
                        <img src="images/20200003.jpg" width="100%"><br>
                        <br><br>
                        <!-- </div> -->
                    </div>
                    <div class="col-md-8 ">
                        <br>
                        <main role="main">


                            <div class="row ">
                                <div class="col">
                                </div>

                                <div class="col-sm-12 border p-3">

                                    <div class="row">
                                        <div class="container1">
                                            <div class="mobile">
                                                <img src="images/20200002.jpg" width="31%">
                                                <img src="images/20200001.jpg" width="31%">
                                                <img src="images/20200003.jpg" width="31%">
                                            </div>
                                            <!-- <div class="container1"> -->
                                            <center>
                                                <h2>แก้ไขข้อมูล</h2>
                                            </center>
                                            <h3>เลือกวิธีการชำระเงิน</h3>
                                            <ul class="breadcrumb">
                                                <li class="active"><a href="order1.php">ส่งฟรี</a></li>
                                                <!--li><a href="order2.php">เก็บปลายทาง</a></li-->
                                            </ul>
                                            <!-- <br> -->
                                            <img src="images/pay-02-1.jpg" width="100%"><br>
    <?php 
         if (isset($errorMsg)) {
    ?>
        <div class="alert alert-danger">
            <strong>Wrong! <?php echo $errorMsg; ?></strong>
        </div>
    <?php } ?>
    

    <?php 
         if (isset($updateMsg)) {
    ?>
        <div class="alert alert-success">
            <strong>Success! <?php echo $updateMsg; ?></strong>
        </div>
    <?php } ?>

                                            <form method="post" autocomplete="off" enctype="multipart/form-data" >
                                                <h3 align="center"> เลือกสินค้า </h3>
                                                <div class="form-row">
                                                    <div class="col-50">
                                                        <label for="cname"><i class="fa fa-shipping-fast"></i> บริการโอน ฟรีค่าจัดส่ง</label>
                                                        <?php echo $promoid; ?>
                                                        <br>
                                                        
                                                        <!-- </div> -->
                                                    </div>
                                                </div>
                                                <!-- </div> -->
                                                <!-- <br> -->
                                                <!-- <div class="col-50 bg-4"> -->
                                                <h3 align="center">ที่อยู่ในการจัดส่ง</h3>
                                                <div class="form-row">
                                                    <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fa fa-user"></i> ชื่อ:</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <?php echo $username; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fa fa-user"></i> นามสกุล:</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <?php echo $lastname; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-50  col-xs-6">
                                                        <label class="text-light"><i class="fab fa-line"></i> Lind ID:</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span> -->
                                                        <?php echo $lineid; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50  col-xs-6">
                                                        <label class="text-light"><i class="fas fa-mobile-alt"></i> เบอร์โทรศัพท์:</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span> -->
                                                        <?php echo $phone; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                </div>
                                               
                                                <div class="form-row">
                                                 <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> บ้านเลขที่</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <?php echo $numhome; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> ซอย</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
														<?php echo $codeems; ?>
                                                        
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> ถนน</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <?php echo $rose; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> หมู่</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
														<?php echo $District2; ?>
                                                        
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> ตำบล/เขต</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
														<?php echo $rovince; ?>
                                                        
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-4">
                                                        <label class="text-light"><i class="fa fa-home"></i> อำเภอ/แขวง</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        
														<?php echo $alley; ?>
                                                        <!-- </div> -->
                                                    </div>  <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fa fa-home"></i> จังหวัด</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
													<?php echo $village; ?>
                                                        
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fa fa-home"></i> รหัสไปรษณีย์</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <?php echo $district; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                   
                                
                                                </div>

                                                <h3 align="center"> แจ้งหลักฐานการโอน </h3>
                                                <div class="form-row">
                                                    <div class="col-50 col-xs-6">
                                                        <label for="cname"><i class="fa fa-landmark"></i> จากธนาคาร</label>
                                                        <?php echo $frombank; ?>
                                                    </div>
                                                    <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fab fa-cc-mastercard"></i> เลขบัญชีต้นทาง</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span> -->
                                                        <?php echo $fromaccount; ?>
                                                        <!-- </div> -->
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-50 col-xs-7">
                                                        <label for="cname"><i class="fa fa-landmark"></i> สู่ธนาคาร/เลขบัญชีปลายทาง</label>
                                                        <?php echo $tobank; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-50 col-xs-5">
                                                        <label for="cvv"><i class="fab fa-cc-visa"></i> ช่องทางการโอน</label>
                                                        <button type="button" class="btn btn-warning px-1" data-toggle="modal" data-target="#exampleModalCenter"> ช่องทางการโอน
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-50">
                                                        <label for="expmonth"><i class="fa fa-photo"></i> สลิปโอนเงิน</label>
                                                        <img src="./img/.<?php echo $row['img']; ?>" width="auto" height="auto" />
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-50 col-xs-6">
                                                        <label class="text-light"><i class="fa fa-user"></i> รหัสไปรษณีย์:</label>
                                                        <!-- <div class="input-group"> -->
                                                        <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> -->
                                                        <input type="text" class="form-control" name="tracks"  maxlength="17"  required placeholder="<?php echo $row['tracks']; ?>">
                                                        <!-- </div> -->
                                                    </div>
                                                <br>
                                                <div class="form-row">
                                                    <div class="col-50">
                                                        <button name="btn_update" type="submit" class="btn btn-success btn-lg btn-block">ยืนยันการการเปลี่ยนข้อมูล</button>
                                                    </div>
                                                </div>
                                                <!-- </div> -->

                                                <br>
                                            </form>
                                            <!-- <div class="mobile">
                                                <img src="images/2802_HULK_0004.jpg" width="31%">
                                                <img src="images/Banner07.5.jpg" width="31%">
                                                <img src="images/Banner06.jpg" width="31%">
                                            </div> -->





                                            <!-- </div> -->
                                            <div class="col">
                                                <br>
                                            </div>
                                        </div>

                        </main>
                    </div>
                    <div class="col-md-2 pc">
                        <br>
                        <img src="images/Hulk_Ads_0002.jpg" width="100%"><br>
                        <img src="images/Ads_Hulk_0001.jpg" width="100%"><br>
                        <img src="images/Ads_2402_0003.jpg" width="100%"><br>
                        <img src="images/20200004.jpg" width="100%"><br>
                        <img src="images/20200005.jpg" width="100%"><br>
                        <br><br>
                    </div>
                    <div class="pc footer1">
  <!-- <p>Footer</p> -->
    <img src="images/Foot2.png" width="100%">
 <p align="center"> Copyright ผลิตภัณฑ์อาหารเสริม เรดวูฟ หมาป่าแดง By redwolfofficial.com  </p>

 <img src="images/Ads_05.jpg" width="19%">
 <img src="images/Ads_02.jpg" width="19%">
 <img src="images/Ads_04.jpg" width="19%">
 <img src="images/Ads_01.jpg" width="19%">
 <img src="images/Ads_03.jpg" width="19%">
  <img src="images/Foot2.png" width="100%">
</div>
<div class="mobile footer2">


  <p align="center">
    <img src="images/Foot2.png" width="100%">
    <!-- <img src="images/images/Foot_Banner.png" width="100%"> -->
  </p>

  <p align="center"> Copyright ผลิตภัณฑ์อาหารเสริม เรดวูฟ หมาป่าแดง By redwolfofficial.com </p>
  <img src="images/Ads_05.jpg" width="18%">
 <img src="images/Ads_02.jpg" width="18%">
 <img src="images/Ads_04.jpg" width="18%">
 <img src="images/Ads_01.jpg" width="18%">
 <img src="images/Ads_03.jpg" width="18%">
</div>                </div>
            </div>
        </div>
          <center>
          <div class="pc footer">
                  <a href="order1.php"><img src="images/00_11.png" width="23%" /></a>
                  <a href="review.php"><img src="images/00_22.png" width="23%"/></a>
                  <a href="tracks.php"><img src="images/00_33.png" width="23%"/></a>
                  <a href="https://lin.ee/7ZwLRpv"><img src="images/00_44.png" width="23%"/></a>
          </div>
  </center>
  <center>
          <div class="mobile footer">
                  <a href="order1.php"><img src="images/01M1.png" width="23%" /></a>
                  <a href="review.php"><img src="images/02M1.png" width="23%"/></a>
                  <a href="tracks.php"><img src="images/03M1.png" width="23%"/></a>
                  <a href="https://lin.ee/7ZwLRpv"><img src="images/04M1.png" width="23%"/></a>
          </div>
  </center>    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">ช่องทางการโอน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                    <img src="https://rabbitfinance.com/images/faq/bill-payment/bank-logo_krungthai.svg" style="width:100px;height:100px;"><br>
                        <td>
                            <h5>นางสาวศิโรรัตน์ แซ่ลี</h5>
                        </td>
                        <td>
                            <h5> 5120806872</h5>
                        </td>
                
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn1 btn-danger" data-dismiss="modal">ปิด</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#promoid').change(function() {
                if ($('#promoid').val() == '1กล่อง350บาท') {
                    $('#1กล่อง350บาท').show();
                } else {
                    $('#1กล่อง350บาท').hide();
                }
            });
        });

        $(document).ready(function() {
            $('#promoid').change(function() {
                if ($('#promoid').val() == '2กล่อง600บาท') {
                    $('#2กล่อง600บาท').show();
                } else {
                    $('#2กล่อง600บาท').hide();
                }
            });
        });

        $(document).ready(function() {
            $('#promoid').change(function() {
                if ($('#promoid').val() == '3กล่อง850บาท') {
                    $('#3กล่อง850บาท').show();
                } else {
                    $('#3กล่อง850บาท').hide();
                }
            });
        });

        $(document).ready(function() {
            $('#promoid').change(function() {
                if ($('#promoid').val() == '4กล่อง1000บาท') {
                    $('#4กล่อง1000บาท').show();
                } else {
                    $('#4กล่อง1000บาท').hide();
                }
            });
        });
    </script>
    <script language="JavaScript">
        function chkNumber(ele) {
            var vchar = String.fromCharCode(event.keyCode);
            if ((vchar < '0' || vchar > '9') && (vchar != '.')) return false;
            ele.onKeyPress = vchar;
        }
    </script>
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>



</body>



</html>
