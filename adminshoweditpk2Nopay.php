<?php  
  require_once('connect.php');

  if (isset($_REQUEST['delete_id'])) {
        $id = $_REQUEST['delete_id'];

        $select_stmt = $db->prepare("SELECT * FROM tb_order2 WHERE id = :id");
        $select_stmt->bindParam(':id', $id);
        $select_stmt->execute();
        $row = $select_stmt->fetch(PDO::FETCH_ASSOC);

        // Delete an original record from db
        $delete_stmt = $db->prepare('DELETE FROM tb_order2 WHERE id = :id');
        $delete_stmt->bindParam(':id', $id);
        $delete_stmt->execute();

        header('Location:adminshoweditpk2Nopay.php');
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script data-ad-client="ca-pub-4542219075817151" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="shortcut icon" href="images/ppp.ico">
<title>admin</title>
</head>
<style type="text/css">
        input[type=search] {width: 500px !important;}
        table.dataTable.display tbody tr.odd {
    background-color: #00000036;
}
table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
    background-color: #00000061;
}ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
body {margin: 0;}

ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

ul.topnav li a:hover:not(.active) {background-color: #111;}

ul.topnav li a.active {background-color: #4CAF50;}

ul.topnav li.right {float: right;}

@media screen and (max-width: 600px) {
  ul.topnav li.right, 
  ul.topnav li {float: none;}
}
 </style>
<body>
<ul class="topnav">
  <li class="text-center"><a  href="adminshoweditpk.php">โอนเงิน</a></li>
  <li><a  class="active" href="adminshoweditpk2Nopay.php">ปลายทาง</a></li>

</ul>
	<div class="container">
  <div class="display-3 text-center">ข้อมูล Admin  </div></br>
      <div class="display-4 text-center">*สำหรับลูกค้า<l style="color:red;">เก็บเงินปลายทาง</l>เท่านั้น *  </div></br>
      <!--a href="add.php" class="btn btn-success mb-3 ">Add+</a-->
<table id="example" class="display table-bordered table-striped">
  <thead>
    <tr>
      <th align="center" scope="col">#</th>
      <th align="center" scope="col">จำนวนสินค้าแลละราคา</th>
      <th width="auto" align="center" scope="col">ชื่อ</th>
      <th align="center" scope="col">นามสกุล</th>
      <th align="center" scope="col">Lind ID</th>
      <th align="center" scope="col">เบอร์โทรศัพท์</th>
      <th align="center" scope="col">รหัสไปรษณีย์</th>
      <th align="center" scope="col">เวลาการสั่งซื้อ</th>
      <th align="center" scope="col">แก้ไข&nbsp;</th>
      <th align="center" scope="col">ลบข้อมูล;</th>
    </tr>
  </thead>
  <tbody>
  <?php 
                $select_stmt = $db->prepare("SELECT * FROM tb_order2");
                $select_stmt->execute();

                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
            ?>
              <tr>
                  
              <th width="auto"><?php echo $row['id']?></th>
                    <td width="auto"><?php echo $row['promoid']?></td>
                  <td width="auto"><?php echo $row['username']?></td>
                  <td width="auto"><?php echo $row['lastname']?></td>
                    <td width="auto"><?php echo $row['lineid']?></td>
                  <td width="auto"><?php echo $row['phone']?></td>
  
                  <div class="lb-outerContainer">
                  <td><?php echo $row["tracks"]; ?></td>
                  <td width="auto"><?php echo $row['time2']?></td>
                    <td width="auto"><a class="btn btn-warning" href="edit4pkSsdkasNopay.php?update_id=<?php echo $row["id"]; ?>">เพิ่มไปรษณีย์</a>
        <!--กรณีส่งค่าไปแก้ไข--></td>
                    <td width="auto"><a class="btn btn-danger" href="?delete_id=<?php echo $row["id"]; ?>" onclick="return confirm('Do you want to delete this record? !!!')"><i class="fa fa-trash-o" aria-hidden="true"></i>
        ลบข้อมูล</a>
        <!--กรณีส่งค่าไปลบ--></td>
                </tr>
                <?php } ?>
  </tbody>
  </table>
 </div></div>
  <script>
		
$(document).ready(function() {
    $('#example').DataTable();
} );
 
	</script>
</body>
</html>