
<?php  
  require_once('connect.php');


?>
<!DOCTYPE html>
<html lang="en">

<head>
	<script data-ad-client="ca-pub-4542219075817151" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   
    <script src="assets/js/jquery.min.js?_=5f06309b462b2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script type="text/javascript" src="ajax/porserver.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css?v=1007" media="screen,projection" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="images/ppp.ico">

    
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <style>
        body {
            font-family: 'thaisansneue';
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th,
        td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }
	
		table#t02 tr:nth-child(odd) {
    background-color: #000000;
}
		table#t02 tr:nth-child(even) {
    background-color: #FFFFFF;
}
.form-control {

    width: 50%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1.7rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.table-dark {
    color:#ffffff;
    
}table.dataTable tbody tr {
    background-color: #982d2d;
}

.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #949494;
}.dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
    color: #fff;
}button, input, select, textarea {
    color: red;
}.table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
  
    background-color: black;
}
    </style>
</head>

<body>
    <p align="center"><img src="images/logo.png" width="40%"></p>

    <div class="bg-1">
        
<div class="topnav" id="myTopnav">
  <div class="col-md-12">
  <a></a>
  <a href="index.php">หน้าแรก</a>
  <a href="review.php">รีวิว</a>
  <a href="promotion.php">โปรโมชั่น</a>
  <a href="order1.php">สั่งซื้อ</a>
  <a href="tracks.php">เลขพัสดุ</a>
  <a href="contact.php">ติดต่อ</a>
  <a href="บทความ.php">บทความ</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div></div>


        <div class="container">
        <div class="col-md-12  bg-3">
            <div class="col-md-12  bg-2">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
   <!-- Indicators -->
   <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>

    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/HULK_WEBSITE_1.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_2.jpg" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="images/HULK_WEBSITE_3.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_4.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_5.jpg"  style="width:100%;">
      </div>

      <div class="item">
        <img src="images/HULK_WEBSITE_6.jpg"  style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
                <div class="col-md-12">
                    <br>
                    <h1 style="text-align:center"><b>เลขพัสดุ</b></h1>
                    <ul class="breadcrumb">
                                                <li class="active"><a href="tracks.php">โอนเงิน</a></li>
                                                <li><a href="tracksNopay.php">เก็บปลายทาง</a></li>
                                            </ul>
                                            <h4 class="text-center">กรุณากรอกข้อมูลวันที่ในการสั่งซื้อให้ครบถ้วนก่อนทำการค้นหาข้อมูล ตัวอย่าง <mark>ปี:2020-เดือน:11-วันที่:11</mark> เพื่อค้นหาข้อมูล</h4>
                     <div class="col-md-12">
                            
                            <div class="col-md-3 "></div>
  
                            <form name="form1" method="post" action="">
                             
                            
                                <p  class="text-center  ">   
                                      
                              <div class="shipping-table">
                                    <div  class="table-responsive">
                                    <table  class="table table-shipping table-striped table-bordered table-dark " id="example">
                                    <thead >
                                    <tr >
                                        <th >#</th>
                                        <th >ชื่อผู้รับสินค้า</th>
                                        <th >นามสกุลผู้รับสินค้า</th>
                                        <th >หมายเลขติดตามสินค้า</th>
                                        <th >เวลาที่ทำการสั่งซื้อ</th>
                                    </tr>
                                    </thead>
												 <?php 
												  $select_stmt = $db->prepare("SELECT * FROM tb_goods");
												  $select_stmt->execute();


												  while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
											  ?>
												<tr>

												<th width="auto"><?php echo $row['id']?></th>

													<td width="auto"><?php echo $row['username']?></td>
													<td width="auto"><?php echo $row['lastname']?></td>
													<td><?php echo $row["tracks"]; ?></td>
													<td width="auto"><?php echo $row['time']?></td>

									
												</tr>
											<?php } ?>
                                    </table>
                                    
                                    </div>
                                    </div>
                                    

                                </div>
                            </div>
                            </form>
                            <div class="col-md-3 "></div>
                            
                       

                    


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '302582137727497');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=302582137727497&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
                    <div class="pc footer1">
  <!-- <p>Footer</p> -->
    <img src="images/Foot2.png" width="100%">
 <p align="center"> Copyright ผลิตภัณฑ์อาหารเสริม เรดวูฟ หมาป่าแดง By redwolpuls.com  </p>

 <img src="images/Ads_05.jpg" width="19%">
 <img src="images/Ads_02.jpg" width="19%">
 <img src="images/Ads_04.jpg" width="19%">
 <img src="images/Ads_01.jpg" width="19%">
 <img src="images/Ads_03.jpg" width="19%">
  <img src="images/Foot2.png" width="100%">
</div>
<div class="mobile footer2">


  <p align="center">
    <img src="images/Foot2.png" width="100%">
    <!-- <img src="images/images/Foot_Banner.png" width="100%"> -->
  </p>

  <p align="center"> Copyright ผลิตภัณฑ์อาหารเสริม เรดวูฟ หมาป่าแดง By redwolpuls.com </p>
  <img src="images/Ads_05.jpg" width="18%">
 <img src="images/Ads_02.jpg" width="18%">
 <img src="images/Ads_04.jpg" width="18%">
 <img src="images/Ads_01.jpg" width="18%">
 <img src="images/Ads_03.jpg" width="18%">
</div>                </div>
            </div>
              <center>
          <div class="pc footer">
                  <a href="order1.php"><img src="images/00_11.png" width="23%" /></a>
                  <a href="review.php"><img src="images/00_22.png" width="23%"/></a>
                  <a href="tracks.php"><img src="images/00_33.png" width="23%"/></a>
                  <a href="https://lin.ee/BUOyPJU"><img src="images/00_44.png" width="23%"/></a>
          </div>
  </center>
  <center>
          <div class="mobile footer">
                  <a href="order1.php"><img src="images/01M1.png" width="23%" /></a>
                  <a href="review.php"><img src="images/02M1.png" width="23%"/></a>
                  <a href="tracks.php"><img src="images/03M1.png" width="23%"/></a>
                  <a href="https://lin.ee/BUOyPJU"><img src="images/04M1.png" width="23%"/></a>
          </div>
  </center>        
		</div>
    </div>



        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script>
          <script>
		
    $(document).ready(function() {
        $('#example').DataTable();
    } );
     
      </script>
</body>

</html>