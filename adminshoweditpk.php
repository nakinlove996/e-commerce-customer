<?php  
  require_once('connect.php');

  if (isset($_REQUEST['delete_id'])) {
    $id = $_REQUEST['delete_id'];

    $select_stmt = $db->prepare("SELECT * FROM tb_goods WHERE id = :id");
    $select_stmt->bindParam(':id', $id);
    $select_stmt->execute();
    $row = $select_stmt->fetch(PDO::FETCH_ASSOC);

    // Delete an original record from db
    $delete_stmt = $db->prepare('DELETE FROM tb_goods WHERE id = :id');
    $delete_stmt->bindParam(':id', $id);
    $delete_stmt->execute();

    header('Location:adminshoweditpk.php');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script data-ad-client="ca-pub-4542219075817151" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="shortcut icon" href="images/ppp.ico">
<title>admin</title>
</head>
<style type="text/css">
        input[type=search] {width: 500px !important;}
        table.dataTable.display tbody tr.odd {
    background-color: #00000036;
}
table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
    background-color: #00000061;
}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
body {margin: 0;}

ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

ul.topnav li {float: left;}

ul.topnav li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

ul.topnav li a:hover:not(.active) {background-color: #111;}

ul.topnav li a.active {background-color: #4CAF50;}

ul.topnav li.right {float: right;}

@media screen and (max-width: 600px) {
  ul.topnav li.right, 
  ul.topnav li {float: none;}
}
 </style>
<body>
<ul class="topnav">
  <li><a class="active" href="adminshoweditpk.php">โอนเงิน</a></li>
  <li><a href="adminshoweditpk2Nopay.php">ปลายทาง</a></li>

</ul>

<div class="container" >
<div class="table">
      <div class="display-3 text-center">ข้อมูล Admin  </div></br>
      <div class="display-4 text-center">*สำหรับลูกค้า<l style="color:red;">โอนเงิน</l>เท่านั้น *  </div></br>
  <!--a href="add.php" class="btn btn-success mb-3 ">Add+</a-->
  
    <table width="100%" class="display table-bordered table-striped table-hover" id="example">
      <thead>
        <tr>
          <th align="center" scope="col">#</th>
          <th align="center" scope="col " >จำนวนสินค้าแลละราคา</th>
          <th width="auto" align="center" scope="col">ชื่อ</th>
          <th align="center" scope="col">นามสกุล</th>
          <th align="center" scope="col"> สลิปโอนเงิน</th>
          <th align="center" scope="col">รหัสcode</th>
          <th align="center" scope="col">เวลาสั่งซื้อ</th>
          <th align="center" scope="col">&nbsp;</th>
          <th align="center" scope="col">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php 
                $select_stmt = $db->prepare("SELECT * FROM tb_goods");
                $select_stmt->execute();
                

                while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)) {
            ?>
              <tr>
                  
                  <th width="auto"><?php echo $row['id']?></th>
                  <td width="auto"><?php echo $row['promoid']?></td>
                  <td width="auto"><?php echo $row['username']?></td>
                  <td width="auto"><?php echo $row['lastname']?></td>
                 

                  
                  <div class="lb-outerContainer">
                    <td width="auto"><img src="./img/.<?php echo $row['img']; ?>" width="140" height="148" /></td>
                    <td><?php echo $row["tracks"]; ?></td>
                    <td width="auto"><?php echo $row['time']?></td>
                    <td width="auto" align="center" valign="middle"><a href="edit4pkSsdkas.php?update_id=<?php echo $row["id"]; ?>" class="btn btn-warning">เพิ่มไปรษณีย์</a>
        <!--กรณีส่งค่าไปแก้ไข--></td>

                    <td width="auto"><a class="btn btn-danger" href="?delete_id=<?php echo $row["id"]; ?>" onclick="return confirm('Do you want to delete this record? !!!')"><i class="fa fa-trash-o" aria-hidden="true"></i>
        ลบข้อมูล</a>
        <!--กรณีส่งค่าไปลบ--></td>
                </tr>
                <?php } ?>
  </tbody>
		
  </table>
 </div>
                </div>
                </div>
  <script>
		
$(document).ready(function() {
    $('#example').DataTable();
} );
 
	</script>
</body>
</html>