const animateCSS = (element, animation) =>
  new Promise((resolve, reject) => {
    const animationName = `${animation}`;
    const node = document.querySelector(element);
    node.classList.add(`animated`, animationName);
    function handleAnimationEnd() {
      node.classList.remove(`animated`, animationName);
      node.removeEventListener("animationend", handleAnimationEnd);

      resolve("Animation ended");
    }
    node.addEventListener("animationend", handleAnimationEnd);
  });
$(document).ready(function() {
  $(".header-btp").click(function() {
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top - 50
      },
      600
    );
    return false;
  });
   $(".bread-nav").click(function(){
    $(".mbnav").toggleClass("open");
  });
  $(".faq-box").click(function() {
    $(this).toggleClass("fopen");
  });
});
var message = new Array();
var reps = 2;
var speed = 250;
var p = message.length;
var T = "";
var C = 0;
var mC = 0;
var s = 0;
var sT = null;
if (reps < 1) reps = 1;

function xnanimate() {
    A()
}

function A() {
    s++;
    if (s > 15) {
        s = 1
    }
    if (s == 1) {
        document.title = 'R '
    }
    if (s == 2) {
        document.title = 'Re_ '
    }
    if (s == 3) {
        document.title = 'Red '
    }
    if (s == 4) {
        document.title = 'RedW_ '
    }
    if (s == 5) {
        document.title = 'RedWo '
    }
    if (s == 6) {
        document.title = 'RedWol_ '
    }
    if (s == 7) {
        document.title = 'RedWolf '
    }
    if (s == 8) {
        document.title = 'RedWol_ '
    }
    if (s == 9) {
        document.title = 'RedWo '
    }
    if (s == 10) {
        document.title = 'RedW_ '
    }
    if (s == 11) {
        document.title = 'Red '
    }
    if (s == 12) {
        document.title = 'Re_ '
    }
    if (s == 13) {
        document.title = 'R '
    }
    if (s == 14) {
        document.title = '_ '
    }
    if (s == 15) {
        document.title = '_ '
    }
    if (C < (15 * reps)) {
        sT = setTimeout("A()", speed);
        C++
    } else {
        C = 0;
        s = 0;
        mC++;
        if (mC > p - 1) mC = 0;
        sT = null;
        xnanimate()
    }
}
xnanimate();
