<html>
<head>
<meta name="viewport" content="width=device-width">
<meta charset="utf-8"><title>MilerDev | Official</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#000000">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#000">
<link rel="alternate" type="application/rss+xml" href="/feed.xml">
<meta name="description" content="A statically generated blog example using Next.js and Markdown.">
<meta property="og:image" content="https://og-image.now.sh/Next.js%20Blog%20Starter%20Example.png?theme=light&amp;md=1&amp;fontSize=100px&amp;images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg"><meta name="next-head-count" content="19"><link rel="preload" href="/_next/static/css/1fe9772657bf1ab98a93.css" as="style"><link rel="stylesheet" href="/_next/static/css/1fe9772657bf1ab98a93.css"><link rel="preload" href="/_next/static/chunks/main-cb7428d18972257ea155.js" as="script"><link rel="preload" href="/_next/static/chunks/webpack-488dc228921f1fdbc0e7.js" as="script"><link rel="preload" href="/_next/static/chunks/framework.d557686129d5a5cc0c94.js" as="script"><link rel="preload" href="/_next/static/chunks/commons.c326fb833cc5579beb78.js" as="script"><link rel="preload" href="/_next/static/chunks/pages/_app-180042e2f0d693eca761.js" as="script"><link rel="preload" href="/_next/static/chunks/510c6f3e6d6c14ee6c7f151ca4326483641314fb.8433a63c2ead412e1d07.js" as="script"><link rel="preload" href="/_next/static/chunks/778b8c8ff2b5c113838eca6b188c79b41ee7b6aa.81f39cf304385b12572e.js" as="script"><link rel="preload" href="/_next/static/chunks/pages/index-a09f8ebd3a9178064f77.js" as="script"><style id="__jsx-3640185409">h2.jsx-3640185409{font-size:1.5rem;}</style><style type="text/css" data-styled-jsx=""></style><link href="/_next/static/chunks/510c6f3e6d6c14ee6c7f151ca4326483641314fb.8433a63c2ead412e1d07.js" rel="prefetch" as="script"><link href="/_next/static/chunks/778b8c8ff2b5c113838eca6b188c79b41ee7b6aa.81f39cf304385b12572e.js" rel="prefetch" as="script"><link href="/_next/static/chunks/pages/index-a09f8ebd3a9178064f77.js" rel="prefetch" as="script"><link href="/_next/static/chunks/pages/blog-4b350f305ae8a5c0f02f.js" rel="prefetch" as="script"><link href="/_next/data/mf-8nexLJ-LYQDSkO0bxo/index.json" rel="prefetch" as="fetch"><link href="/_next/data/mf-8nexLJ-LYQDSkO0bxo/blog.json" rel="prefetch" as="fetch">
</head>
<body>
<section class="courses">
                        <div class="container">
                        <div class="courses-title">
                        <h1>Our Courses</h1><p>คอร์สเรียนของเรา</p></div>
                        <div class="courses-container owl-carousel">
                        <div class="courses-items">
                        <div class="courses-img">
                        <img src="img/img-1.jpg" alt="">
                        </div><div class="courses-info">
                        <h3>Modern HTML &amp; CSS</h3>
                        <p>พื้นฐานสำหรับการเริ่มต้นพัฒนาเว็บไซต์ที่ทุกคนต้องรู้ พร้อมสร้าง 5 โปรเจคแบบจัดเต็ม เรียนจบทุกคนก็สามารถที่จะเริ่มสร้างเว็บไซต์ของตัวเองได้ทันที</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div>
                        <div class="courses-items"><div class="courses-img"><img src="img/img-2.jpg" alt="">
                        </div><div class="courses-info"><h3>PSD to HTML CSS JS + Responsive</h3>
                        <p>เรียนรู้การแปลงไฟล์ .PSD ไปเป็นหน้าเว็บ พร้อมสร้าง 5 โปรเจค และนำความรู้ไปต่อยอดสร้างรายได้ให้ตัวเอง</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div>
                        <div class="courses-items"><div class="courses-img"><img src="img/img-3.jpg" alt=""></div>
                        <div class="courses-info"><h3>Modern JavaScript</h3><p>พื้นฐาน JavaScript ที่นักพัฒนาเว็บไซต์ควรรู้ และพร้อมทำโปรเจคจริง 7 โปรเจค</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div><div class="courses-items">
                        <div class="courses-img"><img src="img/img-4.jpg" alt=""></div><div class="courses-info"><h3>JavaScript30</h3>
                        <p>พาสร้างโปรเจค 30 โปรเจคด้วย JavaScript ทุกคนจะได้เรียนรู้เทคนิคที่สามารถเอาไปประยุกต์ใช้กับเว็บไซต์ของตัวเองได้แบบจัดเต็ม</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div><div class="courses-items">
                        <div class="courses-img"><img src="img/img-5.jpg" alt=""></div><div class="courses-info"><h3>BootStrap 4</h3>
                        <p>เรียนรู้สุดยอด CSS Framework ที่ทั่วโลกนิยมใช้ พร้อมสร้างโปรเจคจริง 5 โปรเจค</p><a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div
                        ><div class="courses-items"><div class="courses-img"><img src="img/img-6.jpg" alt="">
                        </div><div class="courses-info"><h3>PHP &amp; MySQL</h3><p>เรียนรู้ PHP &amp; MySQL พร้อมสร้าง Shopping System เหมาะสำหรับทุกคนที่ต้องการเริ่มต้นเรียน PHP และ MySQL</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div>
                        <div class="courses-items"><div class="courses-img"><img src="img/img-7.jpg" alt=""></div>
                        <div class="courses-info"><h3>React &amp; Redux</h3><p>เรียนรู้ React &amp; Redux ที่เป็น JavaScript library ยอดนิยมของเหล่า Front-end Developer พร้อมสร้างโปรเจคจริง 7 โปรเจค</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a></div></div>
                        <div class="courses-items"><div class="courses-img"><img src="img/img-8.jpg" alt=""></div>
                        <div class="courses-info"><h3>Sass Like A Pro</h3><p>เรียนรู้การพัฒนาเว็บไซต์สมัยใหม่ด้วย Sass แล้วชีวิตการเขียน CSS ของทุกคนจะเปลี่ยนไปตลอดการ พร้อมสร้างโปรเจคจริง 3 โปรเจคแบบจัดเต็ม</p>
                        <a href="https://m.me/patiphan" target="_blank" class="btn">ติดต่อลงทะเบียนเรียน</a>
                        </div></div></div></div>
                        </section>
</body>

</html>